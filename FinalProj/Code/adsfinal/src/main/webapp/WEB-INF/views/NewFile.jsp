<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="com.microsoft.azure.storage.*,com.microsoft.azure.storage.file.*"%>
<html>
<head>
<title>Estimate Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="reptiles Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstarp-css -->
<link href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--// bootstarp-css -->
<!-- css -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/style.css" type="text/css" media="all" />
<!--// css -->
<script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css"/>
  <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/MarkerCluster.css" />
  <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
  <script src="${pageContext.servletContext.contextPath}/resources/leaflet.markercluster.js"></script>
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <style>
    #map{ height: 50% }
    .info {
			padding: 6px 8px;
			font: 14px/16px Arial, Helvetica, sans-serif;
			background: white;
			background: rgba(255,255,255,0.8);
			box-shadow: 0 0 15px rgba(0,0,0,0.2);
			border-radius: 5px;
		}
		.info h4 {
			margin: 0 0 5px;
			color: #777;
		}

		.legend {
			text-align: left;
			line-height: 18px;
			color: #555;
		}
		.legend i {
			width: 18px;
			height: 18px;
			float: left;
			margin-right: 8px;
			opacity: 0.7;
		}
  </style>
</head>
<body   style="background-image:url(<c:url value="/resources/images/bb3.jpg"/>) "   >
	<!-- banner -->
	<div class="banner top-banner">
		<div class="logo">
			<a href="#">Estimated Page</a>
		</div>
		<div class="top-nav">
			<span class="menu">MENU</span>
			<ul class="nav1">
				<li>
				<input  type="button"  value="quick estimate"  id="btn"  onclick="quick();" />
				<!-- <a href="" class="active">Quick Estimate</a> -->
				</li>
				<li>
				<!-- <a href="index.html">Detail Estimate</a> -->
				<input  type="button"  value="detail estimate" id="btn"  onclick="detail();" />
				</li>
			</ul>
		</div>
	</div>


	
     <br /><br />
     	     <br /><br />
     	          <br /><br />

     
	<div id="input" >
	

	
			<div class="contact-info">
			<h1>Please Click the Buttons above to Choose an Estimate Function</h1>
		</div>
	
	
	</div>

	<div id="image"></div>



				<br /><br /><br />\<br />	<br /><br /><br />		<br /><br /><br />\<br />	<br /><br /><br />
			<br /><br /><br />\<br />	<br /><br /><br />
				<br /><br /><br />\<br />	<br /><br /><br />
						<br /><br /><br />\<br />	<br /><br /><br />
			<br /><br /><br />\<br />	<br /><br /><br />
				<br /><br /><br />\<br />	<br /><br /><br />
				



	<script type="text/javascript">
		function Open() {

			var largeImage = document.getElementById('image');
			largeImage.style.display = 'block';
			largeImage.style.width = 200 + "px";
			largeImage.style.height = 200 + "px";
			var url = largeImage.getAttribute('src');
			window
					.open(
							src = "${pageContext.servletContext.contextPath}/resources/images/zip.jpg",
							'Image',
							'width=largeImage.stylewidth,height=largeImage.style.height,resizable=1');

		}


		function dcheck() {
			/* var zip = document.getElementById("zipcode").value;
			var bed = document.getElementById("bedroom").value; */
			var bath = document.getElementById("bathroom").value;

			/* if (!(bed.match("1") || bed.match("2") || bed.match("3")
					|| bed.match("4") || bed.match("5"))) {
				alert("Please enter valid bed room number! Valid bedroom number will be 1-5!");
			} else  */if (!(bath.match("1") || bath.match("1.5")
					|| bath.match("2") || bath.match("2.5") || bath.match("3"))) {
				alert("Please enter valid bath room number! Valid bathroom number will be 1, 1.5, 2, 2.5, 3!");
			}
			else {
				document.forms["detailcheck"].submit();

			}
		}

		function quick() {

			var htmlstr = '<center><div><input class="btn  btn-primary"  type="button" value="Open ZipCode Map" id="btn" onclick="Open();"  />  <br/> <br/>  <br/>';

			htmlstr += '<div class="contact-info" > <h3>Quick Estimate</h3></div>';
			 /* <font size="5" color="8A2BE2">Quick Estimate</font> */

			htmlstr += '<br/>';
			htmlstr += '<br/>';

			htmlstr += '   <form action="estimatequick.htm" method="POST" name="quickcheck">   ';
			htmlstr += '<table class="table table-bordered">';
			
			
			/* -------------- ZIPCODE  --------------*/
			/* 由上个界面传递参数 */
			htmlstr += '<tr class="danger" >';
			htmlstr += '<td align="right" width=50%><font size="3">Zipcode:  </font></td>';
			htmlstr += '<td width=50%> ${requestScope.zipcode } </td>';
			htmlstr += '<input type="hidden" name="zipcode" id="zipcode" value=${requestScope.zipcode }   />';
			htmlstr += '</tr>';

			/* -------------- HOUSING CATEGORY  --------------*/

			htmlstr += '<tr class="success" >';
			htmlstr += '<td align="right" ><font size="3">Housing Category:  </font></td>';
			htmlstr += '<td><input type="radio" name="htype" id="htype" value="2" checked="checked"/>';
			htmlstr += '<font size="3">Apartment </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr  class="warning" >';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="htype" id="htype" value="1"/>';
			htmlstr += '<font size="3">House</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr  class="success">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="htype" id="htype" value="3"/>';
			htmlstr += '<font size="3">Condos</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr  class="warning" >';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="htype" id="htype" value="4"/>';
			htmlstr += '<font size="3">Townhouse</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			/* -------------- ROOM TYPE  --------------*/

			htmlstr += '<tr  class="info" >';
			htmlstr += '<td align="right" ><font size="3">Room Type:  </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="room" id="room" value="studio" checked="checked"/>';
			htmlstr += '<font size="3">Studio </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr   class="active">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="room" id="room" value="1b1b" />';
			htmlstr += '<font size="3">One Bed One Bath </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr class="info" >';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="room" id="room" value="2b1b" />';
			htmlstr += '<font size="3">Two Bed One Bath </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr   class="active">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="room" id="room" value="2b2b" />';
			htmlstr += '<font size="3">Two Bed Two Bath </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr  class="info">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="room" id="room" value="3b1b" />';
			htmlstr += '<font size="3">Three Bed One Bath </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr  class="active">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="room" id="room" value="3b2b" />';
			htmlstr += '<font size="3">Three Bed Two Bath </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr  class="info">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="room" id="room" value="4b2b" />';
			htmlstr += '<font size="3">Four Bed Two Bath </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '</table>';

			htmlstr += '<br/>';
			htmlstr += '<br/>';
			htmlstr += '<td><input  class="btn  btn-primary" type="submit"   value="Quick Estimate" /></td>';

			htmlstr += '</form>    </div> <center/> ';

			document.getElementById("input").innerHTML = htmlstr;

		}

		
		
		
		function detail() {

			var htmlstr = '<center><div><input class="btn  btn-primary"  type="button" value="Open ZipCode Map" id="btn" onclick="Open();"  />  <br/> <br/>  <br/>';

			htmlstr += '<div class="contact-info"> <h3>Detail Estimate</h3></div>';
			/* htmlstr += '<font size="5" color="8A2BE2">Detail Estimate</font>'; */
			htmlstr += '<br/>';
			htmlstr += '<br/>';
			htmlstr += '<form action="estimate.htm" method = "POST" name="detailcheck"> ';
			
			htmlstr += '<table  class="table table-bordered">';
			
			
			/* -------------- ZIPCODE  --------------*/
			/* 由上个界面传递参数 */
			htmlstr += '<tr  class="active">';
			htmlstr += '<td align="right"  width=50% ><font size="3">Zipcode:    </font></td>';
			htmlstr += '<td  width=50%> ${requestScope.zipcode } ';
			htmlstr += '<input type="hidden" name="zipcode" id="zipcode" value=${requestScope.zipcode } />   </td> ';
			htmlstr += '</tr>';
			
			/* --------------  BEDROOM   --------------*/  
			/* 由上个界面传递参数 */
			htmlstr += '<tr  class="success">';
			htmlstr += '<td align="right"><font size="3">Bedroom:   </font></td>';
			htmlstr += '<td> ${requestScope.bed }  ';
			htmlstr += '<input type="hidden" name="bedroom" id="bedroom" value=${requestScope.bed } />   </td> ';
			
			htmlstr += '</tr>';
			
			/* --------------  BATHROOM   --------------*/
			htmlstr += '<tr  class="warning">';
			htmlstr += '<td align="right"><font size="3">Bathroom:    </font></td>';
			htmlstr += '<td><input type="text" name="bathroom" id="bathroom"/></td>';
			htmlstr += '</tr>';
			
			/* --------------  TYPE   --------------*/
			htmlstr += '<tr  class="info">';
			htmlstr += '<td align="right"><font size="3">Type:    </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="type" id="type" value="1" checked="checked"/>';
			htmlstr += '<font size="3">   House </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			

			htmlstr += '<tr  class="info">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="type" id="type" value="2" />';
			htmlstr += '<font size="3">   Apartment</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			

			htmlstr += '<tr  class="info">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="type" id="type" value="3" />';
			htmlstr += '<font size="3">   Condos</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr class="info">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="type" id="type" value="4" />';
			htmlstr += '<font size="3">   Townhouse</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  PARKING   --------------*/
			htmlstr += '<tr  class="warning">';
			htmlstr += '<td align="right"><font size="3">Parking:    </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="parking" id="parking" value="0" checked="checked"/>';
			htmlstr += '<font size="3">   No Parking </font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr  class="warning">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="parking" id="parking" value="1" />';
			htmlstr += '<font size="3">   Indoor Parking</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr class="warning">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="parking" id="parking" value="2" />';
			htmlstr += '<font size="3">   On Street Parking</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  24-EMERGENCY   --------------*/
			htmlstr += '<tr  class="active">';
			htmlstr += '<td align="right"><font size="3">24-Hour Concierge:    </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="emergency" id="emergency" value="0" checked="checked"/>';
			htmlstr += '<font size="3">   No      </font>';
			htmlstr += '<input type="radio" name="emergency" id="emergency" value="1" />';
			htmlstr += '<font size="3">   Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  GYM   --------------*/
			htmlstr += '<tr  class="success">';
			htmlstr += '<td align="right"><font size="3">GYM:   </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="gym" id="gym" value="0" checked="checked"/>';
			htmlstr += '<font size="3">   No     </font>';
			htmlstr += '<input type="radio" name="gym" id="gym" value="1" />';
			htmlstr += '<font size="3">   Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  AC   --------------*/
			htmlstr += '<tr class="warning">';
			htmlstr += '<td align="right"><font size="3">AC:  </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="ac" id="ac" value="0" checked="checked"/>';
			htmlstr += '<font size="3">   No     </font>';
			htmlstr += '<input type="radio" name="ac" id="ac" value="1" />';
			htmlstr += '<font size="3">   Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  WATER-FEE   --------------*/
			htmlstr += '<tr class="danger">';
			htmlstr += '<td align="right"><font size="3">Water-Fee Included:    </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="water" id="water" value="0" checked="checked"/>';
			htmlstr += '<font size="3">   No     </font>';
			htmlstr += '<input type="radio" name="water" id="water" value="1" />';
			htmlstr += '<font size="3">   Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  ELECTRONIC-FEE   --------------*/
			htmlstr += '<tr class="info">';
			htmlstr += '<td align="right"><font size="3">Electronic-Fee Included:    </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="electronic" id="electronic" value="0" checked="checked"/>';
			htmlstr += '<font size="3">   No     </font>';
			htmlstr += '<input type="radio" name="electronic" id="electronic" value="1" />';
			htmlstr += '<font size="3">   Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  HEAT-FEE   --------------*/
			htmlstr += '<tr  class="active">';
			htmlstr += '<td align="right"><font size="3">Heat-Fee Included:   </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="heat" id="heat" value="0" checked="checked"/>';
			htmlstr += '<font size="3">   No     </font>';
			htmlstr += '<input type="radio" name="heat" id="heat" value="1" />';
			htmlstr += '<font size="3">   Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  DISHWASHER   --------------*/
			htmlstr += '<tr  class="success">';
			htmlstr += '<td align="right"><font size="3">Dishwasher Included:    </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="dishwasher" id="dishwasher" value="0" checked="checked"/>';
			htmlstr += '<font size="3">   No     </font>';
			htmlstr += '<input type="radio" name="dishwasher" id="dishwasher" value="1" />';
			htmlstr += '<font size="3">   Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  MICROWAVE   --------------*/
			htmlstr += '<tr  class="warning">';
			htmlstr += '<td align="right"><font size="3">Microwave Included:    </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="microwave" id="microwave" value="0" checked="checked"/>';
			htmlstr += '<font size="3">   No     </font>';
			htmlstr += '<input type="radio" name="microwave" id="microwave" value="1" />';
			htmlstr += '<font size="3">   Yes</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  LAUNDRY   --------------*/
			htmlstr += '<tr class="active">';
			htmlstr += '<td align="right"><font size="3">Laundry:    </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="laundry" id="laundry" value="0" checked="checked"/>';
			htmlstr += '<font size="3">   No</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			htmlstr += '<tr class="active">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="laundry" id="laundry" value="1" />';
			htmlstr += '<font size="3">   In Unit</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr class="active">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="laundry" id="laundry" value="2" />';
			htmlstr += '<font size="3">   In Building</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';
			
			/* --------------  TRAVEL   --------------*/
			htmlstr += '<tr class="success">';
			htmlstr += '<td align="right"><font size="3">Walk To Public Transportation:   </font></td>';
			htmlstr += '<td>';
			htmlstr += '<input type="radio" name="travel" id="travel" value="1" checked="checked"/>';
			htmlstr += '<font size="3">   Within 10 Mins</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr class="success">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="travel" id="travel" value="2" />';
			htmlstr += '<font size="3">   10-20 Mins</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '<tr class="success">';
			htmlstr += '<td></td><td>';
			htmlstr += '<input type="radio" name="travel" id="travel" value="3" />';
			htmlstr += '<font size="3">   More Than 20 Mins</font>';
			htmlstr += '</td>';
			htmlstr += '</tr>';

			htmlstr += '</table>';
			htmlstr += '<br/>';
			htmlstr += '<br/>';
			
			htmlstr += '<input class="btn  btn-primary"  type="button"   value="Detail Estimate"  onclick="dcheck()" />';
			htmlstr += '</form>  </div> <center/>';

			document.getElementById("input").innerHTML = htmlstr;
		}
		
	</script>


</body>
</html>

<%-- 	<div>

		<div id="body_head">

			<!-- Carousel ================================================== -->
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" 
					    data-slide-to="0" class="active"></li>
					    
					<li data-target="#myCarousel" 
					    data-slide-to="1"></li>
					    
					<li data-target="#myCarousel" 
					    data-slide-to="2"></li>
					    
					<li data-target="#myCarousel" 
					    data-slide-to="3"></li>
				</ol>
				
				<div class="carousel-inner" 
				     role="listbox">

					<div class="item active">
						<img class="first-slide"
							src="${pageContext.servletContext.contextPath}/resources/images/l5.jpeg"
							alt="First slide" height=40% width=100%>
						<div class="container">

							<div class="carousel-caption">

								<h1>Team Four Final Project</h1>
								<p></p>
								<p></p>
							</div>
						</div>
					</div>


					<div class="item">
						<img class="second-slide"
							src="${pageContext.servletContext.contextPath}/resources/images/l4.jpg"
							alt="Second slide" height=40% width=100%>
						<div class="container">
							<div class="carousel-caption">
								<h1>Wenjin Cao, Jianxing Lu, Qiaomin Ling</h1>
								<p></p>
								<p></p>
								<p></p>

							</div>
						</div>
					</div>


					<div class="item">
						<img class="third-slide"
							src="${pageContext.servletContext.contextPath}/resources/images/l7.jpg"
							alt="Third slide" height=40% width=100%>
						<div class="container">
							<div class="carousel-caption">
								<!-- <h1>Quick Estimate</h1> -->
								<input class="btn  btn-outline-secondary  btn-lg" type="button"
									value="quick estimate" id="btn" onclick="quick();" /><br />
								<p></p>
								<p></p>
								<p></p>
							</div>
						</div>
					</div>


					<div class="item">
						<img class="fourth-slide"
							src="${pageContext.servletContext.contextPath}/resources/images/l3.jpg"
							alt="Fourth slide" height=40% width=100%>
						<div class="container">
							<div class="carousel-caption">
								<!-- <h1>Detail Estimate</h1> -->
								<input class="btn  btn-outline-secondary  btn-lg" type="button"
									value="detail estimate" id="btn" onclick="detail();" /> <br />
								<p></p>
								<p></p>
								<p></p>

							</div>
						</div>
					</div>

				</div>
				<a class="left carousel-control" href="#myCarousel" role="button"
					data-slide="prev"> <span
					class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a> <a class="right carousel-control" href="#myCarousel" role="button"
					data-slide="next"> <span
					class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
			<!-- /.carousel ================================================================-->

		</div>


	</div> --%>


	