<%-- 
	This is the ${requestScope.estimatedtype} <br/>
	estimate result: ${requestScope.result} 
 --%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="com.microsoft.azure.storage.*,com.microsoft.azure.storage.file.*"%>
<html>
<head>
<title>Estimate Result</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="reptiles Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstarp-css -->
<link href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--// bootstarp-css -->
<!-- css -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/style.css" type="text/css" media="all" />
<!--// css -->
<script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css"/>
  <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/MarkerCluster.css" />
  <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
  <script src="${pageContext.servletContext.contextPath}/resources/leaflet.markercluster.js"></script>
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <style>
    #map{ height: 50% }
    .info {
			padding: 6px 8px;
			font: 14px/16px Arial, Helvetica, sans-serif;
			background: white;
			background: rgba(255,255,255,0.8);
			box-shadow: 0 0 15px rgba(0,0,0,0.2);
			border-radius: 5px;
		}
		.info h4 {
			margin: 0 0 5px;
			color: #777;
		}

		.legend {
			text-align: left;
			line-height: 18px;
			color: #555;
		}
		.legend i {
			width: 18px;
			height: 18px;
			float: left;
			margin-right: 8px;
			opacity: 0.7;
		}
  </style>
</head>
<body    >
	<!-- banner -->
	<div class="banner top-banner">
		<div class="logo">
			<a href=""></a>
		</div>
		<div class="top-nav">
			<span class="menu">MENU</span>
			<ul class="nav1">
				<!-- <li><a href="" class="active">Map</a></li> -->
				<!-- <li><a href="index.html">Home</a></li> -->
			</ul>
			<!-- script-for-menu -->

			<!-- /script-for-menu -->
		</div>
	</div>
	<!-- //banner -->
	<!-- contact -->
	<div class="contact-top">
		<!-- container -->
		<div class="container">
			<div class="contact-info">
			
			
			<p class="caption"><b>This is </b></p>
			<p class="caption"><b>${requestScope.estimatedtype}</b></p>
			<h3>Estimate Result:<br/>  ${requestScope.result} </h3>
			<h1> </h1>
			
			
			 
			 </div>
			</div>
			</div>
			</body>
			
			
			
			</html>