<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd"><%@ page import="java.awt.*"%>


<%@ page import="org.springframework.web.multipart.MultipartFile"%>
<%@ page import="org.apache.commons.io.FileUtils"%>
<%@ page import="org.apache.commons.io.FilenameUtils"%>















<html>
<head>
<script
	src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.js"></script>
</head>
</head>
<body>
	<nav class="navbar navbar-default style="background-color: #52cfeb ;" navbar-static-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href=''>Home Page</a></li>
				<li class=""><a
					href='https://app.powerbi.com/groups/me/dashboards/65118f7b-a6ef-4b07-80e9-2f72ab44c3b4'>Real-time
						Azure Stream Analytics</a></li>
			</ul>
		</div>
	</div>
	</nav>

	Total
	<font size="5" color="0055ff">${requestScope.totalnumber}</font>
	Twitters
	<br /> result:
	<font size="5" color="0055ff">${requestScope.finalAnswer}</font>
	<br />








	<br />

	<br />

	<br />

	<!-- #######################Pie Chart############################################# -->

	<input type="hidden" id="pos" name="pos" value="${requestScope.pos}" />

	<input type="hidden" id="neg" name="neg" value="${requestScope.neg}" />

	<input type="hidden" id="nel" name="nel" value="${requestScope.nal}" />

	<canvas id="pieChart"> </canvas>

	<script>
		var ctx = document.getElementById("pieChart");

		ctx.height = 150;

		ctx.weight = 100;

		var pos = document.getElementById("pos").value;

		var neg = document.getElementById("neg").value;

		var nel = document.getElementById("nel").value;

		var myChart = new Chart(

		ctx, {

			type : 'pie',

			data : {

				labels : [ "Negative", "Neutral", "Positive" ],

				datasets : [ {

					label : 'label',

					data : [ nel, neg, pos ],

					backgroundColor : [ 'rgba(255, 99, 132, 0.2)',
							'rgba(54, 162, 235, 0.2)',
							'rgba(255, 206, 86, 0.2)' ],

					borderColor : [ 'rgba(255,99,132,1)',
							'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)' ],

					borderWidth : 1

				} ]

			},

			options : {
				scales : {
					yAxes : [ {
						ticks : {
							beginAtZero : true
						}
					} ]
				}
			}

		});
	</script>



	<br />
	<br />
	<br />
	<br />
	<br />

	<!-- #######################Line Chart############################################# -->

	<input type="hidden" id="p1" name="p1" value="${requestScope.point1}" />

	<input type="hidden" id="p2" name="p2" value="${requestScope.point2}" />

	<input type="hidden" id="p3" name="p3" value="${requestScope.point3}" />

	<input type="hidden" id="p4" name="p4" value="${requestScope.point4}" />

	<input type="hidden" id="p5" name="p5" value="${requestScope.point5}" />

	<input type="hidden" id="p6" name="p6" value="${requestScope.point6}" />

	<input type="hidden" id="p7" name="p7" value="${requestScope.point7}" />

	<input type="hidden" id="p8" name="p8" value="${requestScope.point8}" />

	<input type="hidden" id="p9" name="p9" value="${requestScope.point9}" />

	<input type="hidden" id="p10" name="p10"
		value="${requestScope.point10}" />

	<canvas id="barChart"> </canvas>

	<script>
		var ctx = document.getElementById("barChart");

		ctx.height = 150;

		ctx.weight = 100;

		var pos = document.getElementById("pos").value;

		var p1 = document.getElementById("p1").value;

		var p2 = document.getElementById("p2").value;

		var p3 = document.getElementById("p3").value;

		var p4 = document.getElementById("p4").value;

		var p5 = document.getElementById("p5").value;

		var p6 = document.getElementById("p6").value;

		var p7 = document.getElementById("p7").value;

		var p8 = document.getElementById("p8").value;

		var p9 = document.getElementById("p9").value;

		var p10 = document.getElementById("p10").value;

		var myChart = new Chart(ctx,
				{

					type : 'bar',

					data : {

						labels : [ "0-0.1", "0.1-0.2", "0.2-0.3", "0.3-0.4",
								"0.4-0.5", "0.5-0.6", "0.6-0.7", "0.7-0.8",
								"0.8-0.9", "0.9-1.0" ],

						datasets : [ {

							label : [ "Sentiment Score Bar Chart" ],

							data : [ p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 ],

							backgroundColor : [ 'rgba(75,0,145,0.8)',
									'rgba(139,90,184,0.8)',
									'rgba(185,156,212,0.8)',

									'rgba(220,205,233,0.8)',
									'rgba(241,235,246,0.8)',
									'rgba(245,225,225,0.8)',

									'rgba(250,205,205,0.8)',
									'rgba(244,132,132,0.8)',
									'rgba(238,50,50,0.8)',

									'rgba(234,0,0,0.8)' ],

							borderColor : [ 'rgba(75,0,145,0.8)',
									'rgba(139,90,184,0.8)',
									'rgba(185,156,212,0.8)',

									'rgba(220,205,233,0.8)',
									'rgba(241,235,246,0.8)',
									'rgba(255,255,255,0.8)',

									'rgba(250,205,205,0.8)',
									'rgba(244,132,132,0.8)',
									'rgba(238,50,50,0.8)',

									'rgba(234,0,0,0.8)' ],

							borderWidth : 1

						} ]

					},

					options : {

						scales : {

							yAxes : [ {

								ticks : {

									beginAtZero : true

								}

							} ]

						}

					}

				});
	</script>





	<br />
	<br />
	<br />
	<br />
	<br />





	<!-- #######################Line Chart############################################# -->

	<%-- <input type="hidden" id="d1" name="d1" value="${requestScope.nel }" />

<input type="hidden" id="d2" name="d2" value="${requestScope.nel}" />

<input type="hidden" id="d3" name="d3" value="${requestScope.nel}" />

<input type="hidden" id="d4" name="d4" value="${requestScope.pos}" />

<input type="hidden" id="d5" name="d5" value="${requestScope.neg}" />

<input type="hidden" id="d6" name="d6" value="${requestScope.nel}" />

<input type="hidden" id="d7" name="d7" value="${requestScope.pos}" />

<canvas id="lineChart" > 

</canvas>

        <script>

            var ctx = document.getElementById("lineChart");

            ctx.height = 150;

            ctx.weight = 100; 

            var pos = document.getElementById("pos").value;

            

            var d1 = document.getElementById("d1").value;

            var d2 = document.getElementById("d2").value;

            var d3 = document.getElementById("d3").value;

            var d4 = document.getElementById("d4").value;

            var d5 = document.getElementById("d5").value;

            var d6 = document.getElementById("d6").value;

            var d7 = document.getElementById("d7").value;

            

            var myChart = new Chart(ctx, {

                type : 'line',

                data : {

                    labels : [ "Day1","Day2","Day3","Day4","Day5", "Day6","Day7"],

                    

                    datasets : [ {

                        label : ["Sentiment Line Bar Chart"],

                        

                        data : [ d1,d2,d3,d4,d5,d6,d7],

                        backgroundColor : [ 'rgba(75,0,145,0.8)','rgba(139,90,184,0.8)',  'rgba(185,156,212,0.8)',

                                            'rgba(220,205,233,0.8)','rgba(241,235,246,0.8)',  'rgba(245,225,225,0.8)',

                                            'rgba(250,205,205,0.8)'],

                        borderColor : [ 'rgba(75,0,145,0.8)','rgba(139,90,184,0.8)',  'rgba(185,156,212,0.8)',

                                        'rgba(220,205,233,0.8)','rgba(241,235,246,0.8)',  'rgba(255,255,255,0.8)',

                                        'rgba(250,205,205,0.8)'],

                        borderWidth : 1

                    } ]

                },

                

                options : {

                    scales : {

                        yAxes : [ {

                            

                            

                             ticks : {

                                beginAtZero : true

                            }

                        } ]

                    }

                }

            });

        </script> --%>




</body>
</html>


