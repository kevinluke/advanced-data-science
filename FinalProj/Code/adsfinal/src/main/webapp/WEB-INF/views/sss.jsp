<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
<head>
<title>FinalProject</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="reptiles Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstarp-css -->
<link href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--// bootstarp-css -->
<!-- css -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/style.css" type="text/css" media="all" />
<!--// css -->
<script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css"/>
  <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/MarkerCluster.css" />
  <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
  <script src="${pageContext.servletContext.contextPath}/resources/leaflet.markercluster.js"></script>
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <style>
    #map{ height: 50% }
  </style>
</head>
<body>
	<!-- banner -->
	<div class="banner top-banner">
		<div class="logo">
			<a href="index.html">TEAM FOUR</a>
		</div>
		<div class="top-nav">
			<span class="menu">MENU</span>
			<ul class="nav1">
				<li><a href="" class="active">Map</a></li>
				<li><a href="index.html">Home</a></li>
			</ul>
			<!-- script-for-menu -->
				<script>
					 $( "span.menu" ).click(function() {
					$( "ul.nav1" ).slideToggle( 300, function() {
					// Animation complete.
						});
						});
				</script>
			<!-- /script-for-menu -->
		</div>
	</div>
	<!-- //banner -->
	<!-- contact -->
	<div class="contact-top">
		<!-- container -->
		<div class="container">
			<div class="contact-info">
				<h3>Price and Details Map</h3>
				<p class="caption">you can easily check the heatmap of rent price in Boston and any house information in details</p>
			</div>
			<div id="map" class="map footer-middle">
				
			</div>
			<script>
var map = L.map('map').setView([42.35, -71.08], 13);
  
  // load a t;ile layer
L.tileLayer('http://tiles.mapc.org/basemap/{z}/{x}/{y}.png',
	    {
	      attribution: 'Tiles by <a href="http://mapc.org">MAPC</a>, Data by <a href="http://www.zillow.com/research/data/#rental-data">Zillow</a>',
	      maxZoom: 17,
	      minZoom: 9
	    }).addTo(map);
function remap(){
	map.remove();
	map = L.map('map').setView([42.35, -71.08], 13);
	  L.tileLayer('http://tiles.mapc.org/basemap/{z}/{x}/{y}.png',
	    {
	      attribution: 'Tiles by <a href="http://mapc.org">MAPC</a>, Data by <a href="http://mass.gov/mgis">MassGIS</a>',
	      maxZoom: 17,
	      minZoom: 9
	    }).addTo(map);
}
  function test(){
	  map.remove();
	  // load a t;ile layer
	  map = L.map('map').setView([42.35, -71.08], 13);
	  L.tileLayer('http://tiles.mapc.org/basemap/{z}/{x}/{y}.png',
	    {
	      attribution: 'Tiles by <a href="http://mapc.org">MAPC</a>, Data by <a href="http://mass.gov/mgis">MassGIS</a>',
	      maxZoom: 17,
	      minZoom: 9
	    }).addTo(map);
  // initialize the map
  
	  var folder = '<%=request.getContextPath()%>' ;
	  var txt = document.getElementById("txt").value;
	  var year = document.getElementById("year").value;
	  var bed = document.getElementById("bed").value;
	  txt+=".geojson";
	  year+=bed;
	  year+=".geojson";
	  
	
  $.getJSON(folder+"/resources/"+year,function(hoodData){
    L.geoJson( hoodData, {
      style: function(feature){
        var fillColor,
            density = feature.properties.density;
        if ( density > 3500 ) fillColor = "#005a32";
        else if ( density > 3000 ) fillColor = "#238b45";
        else if ( density > 2500 ) fillColor = "#41ab5d";
        else if ( density > 2000 ) fillColor = "#74c476";
        else if ( density > 1500 ) fillColor = "#a1d99b";
        else if ( density > 1000 ) fillColor = "#c7e9c0";
        else if ( density > 0 ) fillColor = "#e5f5e0";
        else fillColor = "#f7fcf5";  // no data
        return { color: "#999", weight: 1, fillColor: fillColor, fillOpacity: .6 };
      },
      onEachFeature: function( feature, layer ){
    	  if(feature.properties.density=="0"){
    		  layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: Unavailable <br/>"+
    	        		"<form action=\"ViewDetails.htm\" method=\"post\"><input name=\"details\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input type=\"submit\" value=\"details\"></form></div>");

    	  }
    	  else{
        layer.bindPopup( "<div class=\"contact-form\"><strong>" + feature.properties.Name + "</strong><br/>" + "Median Price: $ "+feature.properties.density +"<br/>"+
        		"<form action=\"ViewDetails.htm\" method=\"post\"><input name=\"details\" type=\"hidden\" value=\""+feature.properties.Name+"\"><input type=\"submit\" value=\"details\"></form></div>");
    	  }
      }
    }).addTo(map);
  });

  $.getJSON(folder+"/resources/"+txt,function(data){
    var ratIcon = L.icon({
      iconUrl: 'http://humanrights.gradeeight.org/homelessnessca/Home.png',
      iconSize: [60,50]
    });
    var rodents = L.geoJson(data,{
      pointToLayer: function(feature,latlng){
        var marker = L.marker(latlng,{icon: ratIcon});
        marker.bindPopup('Bedroom: '+feature.properties.Bedroom + '<br/>' + 'Price: '+feature.properties.Price);
        return marker;
      }
    });
    var clusters = L.markerClusterGroup();
    clusters.addLayer(rodents);
    map.addLayer(clusters);
  });
  var legend = L.control({position: 'bottomright'});

  legend.onAdd = function (map) {

      var div = L.DomUtil.create('div', 'info legend'),
          grades = [0, 10, 20, 50, 100, 200, 500, 1000],
          labels = [];

      // loop through our density intervals and generate a label with a colored square for each interval
      for (var i = 0; i < grades.length; i++) {
          div.innerHTML +=
              '<i style="background:' + getColor(grades[i] + 1) + '"></i> ' +
              grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
      }

      return div;
  };

  legend.addTo(map);
 }
 
  </script>
			<div class="mail-grids">
				<div class="col-md-6 mail-grid-left wow fadeInLeft animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
					<h3>Intro</h3>
					<h5>put some introductions here <span>and here </span></h5>
					<h4>Team members</h4>
					<p>Jianxing Lu
						<span>Qiaomin Ling</span>
						Wenjin Cao
					</p>
					<h4>Get In Touch</h4>
					<p>Telephone: +1 234 567 9871
						<span>FAX: +1 234 567 9871</span>
						E-mail: <a href="mailto:info@example.com">mail@example.com</a>
					</p>
				</div>
				<div class="col-md-6 contact-form">
				<h3>Option</h3>
					<form>
					<h5>Select month to check every rent info</h5><br/>
						<select id="txt" name="txt">
		  <option value ="2013-02-18" >2013-02-18</option>
		  <option value ="2013-03-18">2013-03-18</option>
		  <option value ="2013-04-18">2013-04-18</option>
		  <option value ="2013-05-18">2013-05-18</option>
		  <option value ="2013-06-18">2013-06-18</option>
		  <option value ="2013-07-18">2013-07-18</option>
		  <option value ="2013-08-18">2013-08-18</option>
		  <option value ="2013-09-18">2013-09-18</option>
		  <option value ="2013-10-18">2013-10-18</option>
		  <option value ="2013-11-18">2013-11-18</option>
		  <option value ="2013-11-21">2013-11-21</option>
		  <option value ="2013-12-18">2013-12-18</option>
		  <option value ="2014-01-18">2014-01-18</option>
		  <option value ="2014-02-18">2014-02-18</option>
		  <option value ="2014-03-18">2014-03-18</option>
		  <option value ="2014-04-18">2014-04-18</option>
		  <option value ="2014-05-18">2014-05-18</option>
		  <option value ="2014-06-18">2014-06-18</option>
		  <option value ="2014-07-18">2014-07-18</option>
		  <option value ="2014-08-18">2014-08-18</option>
		  <option value ="2014-09-18">2014-09-18</option>
		  <option value ="2014-10-18">2014-10-18</option>
		  <option value ="2014-11-18">2014-11-18</option>
		  <option value ="2014-12-18">2014-12-18</option>
		  <option value ="2015-01-18">2015-01-18</option>
		  <option value ="2015-02-18">2015-02-18</option>
		  <option value ="2015-03-18">2015-03-18</option>
		  <option value ="2015-04-18">2015-04-18</option>
		  <option value ="2015-05-18">2015-05-18</option>
		  <option value ="2015-06-18">2015-06-18</option>
		  <option value ="2015-07-18">2015-07-18</option>
		  <option value ="2015-08-18">2015-08-18</option>
		  <option value ="2015-09-18">2015-09-18</option>
		  <option value ="2015-10-18">2015-10-18</option>
		  <option value ="2015-11-18">2015-11-18</option>
		  <option value ="2015-12-18">2015-12-18</option>
		  <option value ="2016-01-18">2016-01-18</option>
		  <option value ="2016-02-18">2016-02-18</option>
		  <option value ="2016-03-18">2016-03-18</option>
		  <option value ="2016-04-18">2016-04-18</option>
		  <option value ="2016-05-18">2016-05-18</option>
		  <option value ="2016-06-18">2016-06-18</option>
		  <option value ="2016-07-18">2016-07-18</option>
		</select><br>
		<h5>Select year to check heat map</h5><br/>
		<select id="year" name="year">
		  <option value ="2013" >2013</option>
		  <option value ="2014" >2014</option>
		  <option value ="2015" >2015</option>
		  <option value ="2016" >2016</option>
		  <option value ="2016o" >2016(Ourselves)</option>
		</select>
		<h5>Select number of bedroom</h5><br/>
		<select id="bed" name="bed">
		  <option value ="0" >Studio</option>
			<option value ="1" >1 Bedroom</option>
			<option value ="2" >2 Bedroom</option>
			<option value ="3" >3 Bedroom</option>
			<option value ="4" >4 Bedroom</option>
			<option value ="5" >5 Bedroom</option>
		</select>
		<input type="button" value="View in map" onclick="test()"/>
		<input type="button" value="Refresh map" onclick="remap()"/>
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<!-- //container -->
	</div>

	<!-- //contact -->
	<!-- footer -->
	<div class="footer">
		<!-- container -->
		<div class="container">
			<div class="footer-top">
				<div class="footer-logo">
					<a href="index.html">TEAM FOUR</a>
				</div>
				<div class="footer-icons">
					<ul>
						<li><a href="#" class="facebook"> </a></li>
						<li><a href="#" class="facebook twitter"> </a></li>
						<li><a href="#" class="facebook chrome"> </a></li>
						<li><a href="#" class="facebook dribbble"> </a></li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<!-- //container -->
		
	</div>
	<!-- //footer -->
</body>	
</html>