<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<script
	src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link href="resources/css/carousel.css" rel="stylesheet">
<link rel="stylesheet" href="css/searchBar.css">

<meta charset="utf-8">
<title>Smoothie Charts: Ten Minute Tutorial</title>
<script async src="http://c.cnzz.com/core.php"></script>
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="skeleton/base.css">
<link rel="stylesheet" href="skeleton/skeleton.css">
<link rel="stylesheet" href="skeleton/layout.css">
<link rel="stylesheet" href="font-awesome/css/font-awesome.css">
<link rel="stylesheet" href="smoothie-website.css">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600"
	rel="stylesheet">
<script type="text/javascript"
	src="https://raw.githubusercontent.com/joewalnes/smoothie/master/smoothie.js"></script>
<link
	href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen"
	href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
<script type="text/javascript"
	src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript"
	src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js"></script>

<title>Home</title>

</head>
<body>
	<div>
		<div id="body_head">
			<nav class="navbar navbar-default style="background-color: #52cfeb ;" navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse" data-target="#navbar"
							aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li class="active"><a href=''>Home Page</a></li>
							<li class=""><a
								href='https://app.powerbi.com/groups/me/dashboards/65118f7b-a6ef-4b07-80e9-2f72ab44c3b4'>Real-time
									Azure Stream Analytics</a></li>
						</ul>
					</div>
				</div>
			</nav>

			<!-- Carousel ================================================== -->
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
				</ol>
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<img class="first-slide"
							src="${pageContext.servletContext.contextPath}/resources/images/t1.jpg"
							alt="First slide">
						<div class="container">

							<div class="carousel-caption">

								<h1>Twitter Sentiment Analysis.</h1>
								<p></p>
								<p></p>
							</div>
						</div>
					</div>
					<div class="item">
						<img class="second-slide"
							src="${pageContext.servletContext.contextPath}/resources/images/13.jpg"
							alt="Second slide" height="" width="">
						<div class="container">
							<div class="carousel-caption">
								<h1>Social Media Analytics.</h1>
								<p></p>

							</div>
						</div>
					</div>
					<div class="item">
						<img class="third-slide"
							src="${pageContext.servletContext.contextPath}/resources/images/11.png"
							alt="Second slide" height="" width="">
						<div class="container">
							<div class="carousel-caption">
								<h1>Team 4.</h1>
								<p></p>

							</div>
						</div>
					</div>

				</div>
				<a class="left carousel-control" href="#myCarousel" role="button"
					data-slide="prev"> <span
					class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a> <a class="right carousel-control" href="#myCarousel" role="button"
					data-slide="next"> <span
					class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
			<!-- /.carousel ================================================================-->

		</div>

		<div class="container">
			<table>
				<tr>
					<td>
						<form action="testdate.htm" method="POST">
							<h3>Search tweets of a time period</h3>
							<div id="datetimepicker" class="input-append date">

								<input type="text" name="tdate"
									placeholder="Select a beginning time"></input> <span
									class="add-on"> <i data-time-icon="icon-time"
									data-date-icon="icon-calendar"></i>
								</span>
							</div>
							<script type="text/javascript">
								$('#datetimepicker').datetimepicker({
									format : 'yyyy-MM-dd',
									language : 'en',
									pickDate : true,
									hourStep : 1,
									minuteStep : 15,
									secondStep : 30,
									inputMask : true
								});
							</script>

							<div id="datetimepicker2" class="input-append date">

								<input type="text" name="udate" placeholder="Select an end time"></input>
								<span class="add-on"> <i data-time-icon="icon-time"
									data-date-icon="icon-calendar"></i>
								</span>
							</div>
							<script type="text/javascript">
								$('#datetimepicker2').datetimepicker({
									format : 'yyyy-MM-dd',
									language : 'en',
									pickDate : true,
									hourStep : 1,
									minuteStep : 15,
									secondStep : 30,
									inputMask : true
								});
							</script>
							<input name="keyword" type="text"
								placeholder="Input a key word to search" /> <input type="submit"
								value="Search" />
						</form>
					</td>
					<td><img
						src="${pageContext.servletContext.contextPath}/resources/images/t8.png"
						alt="" height="" width=""></td>
					<td>
						<form action="SearchTwitter.htm" method="POST">
							<h3>Search tweets of a day</h3>
							<div id="datetimepicker3" class="input-append date">

								<input type="text" name="ddate" placeholder="Select a day"></input> <span class="add-on">
									<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
								</span>
							</div>
							<input type="text" name="keywordtest" id="keywordtest"
								placeholder="Input a key word to search" />

							<script type="text/javascript">
								$('#datetimepicker3').datetimepicker({
									format : 'yyyy-MM-dd',
									language : 'en',
									pickDate : true,
									hourStep : 1,
									minuteStep : 15,
									secondStep : 30,
									inputMask : true
								});
							</script>
							<input type="submit" value="Search" />
						</form>

					</td>
				</tr>

			</table>
		</div>



		<!-- <input name="key" id="keyword" type="text"/>
<input type="button" name="button" id="button" value="Submit" onclick="confirm()"/>
<script type="text/javascript">
	function confirm()
	{
		var key = document.getElementById("keyword").value;

		var xmlhttp;
        if (window.XMLHttpRequest){
            xmlhttp=new XMLHttpRequest();
        }
        else{
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
            if (xmlhttp.readyState==4 && xmlhttp.status==200){
            	test(xmlhttp.responseText);
            }
          }
        xmlhttp.open("GET","test.htm?keyword="+key,true);
        xmlhttp.send();
	}
	</script>
<script type="text/javascript">
	function test(res){
		alert(res);
		var reses = res.split('~!@');
		/* for(var i=0;i<reses.length;i++){
			alert(reses[i]);
		} */
	}
	</script> -->

		<!-- <script> -->





		<%-- <h2>test 1</h2>
<div class="holder"><canvas id="mycanvas3" width="400" height="100"></canvas></div>
        <script type="text/javascript">
          (function() {
            var smoothie = new SmoothieChart();
            smoothie.streamTo(document.getElementById("mycanvas3"));
          })();
        </script>
<P>  The time on the server is ${serverTime}. </P>
<h2>test 2</h2>
<div class="holder"><canvas id="mycanvas4" width="400" height="100"></canvas></div>
        <script type="text/javascript">
          (function() {
            var line1 = new TimeSeries();
            var line2 = new TimeSeries();
            setInterval(function() {
              line1.append(new Date().getTime(), Math.random());
              line2.append(new Date().getTime(), Math.random());
            }, 1000);

            var smoothie = new SmoothieChart();
            smoothie.addTimeSeries(line1);
            smoothie.addTimeSeries(line2);
            smoothie.streamTo(document.getElementById("mycanvas4"),1000);
          })();
        </script>
<h2>test 3</h2>
<div class="holder"><canvas id="mycanvas5" width="400" height="100"></canvas></div>
        <script type="text/javascript">
          (function() {
            var line1 = new TimeSeries();
            setInterval(function() {
              line1.append(new Date().getTime(), Math.random()*5);
            }, 1000);

            var smoothie = new SmoothieChart({
                grid: { strokeStyle: 'rgb(125, 0, 0)', fillStyle: 'rgb(60, 0, 0)', lineWidth: 1, millisPerLine: 250, verticalSections: 6 },
                labels: { fillStyle:'rgb(255, 255, 0)' }
            });
            smoothie.addTimeSeries(line1, { strokeStyle: 'rgb(0, 255, 0)', fillStyle: 'rgba(0, 255, 0, 0.4)', lineWidth: 3 });
            smoothie.streamTo(document.getElementById("mycanvas5"));
          })();
        </script> --%>

	</div>
</body>
</html>
