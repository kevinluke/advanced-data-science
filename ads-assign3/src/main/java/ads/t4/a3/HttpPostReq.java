package ads.t4.a3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;



import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
//import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

import com.cedarsoftware.util.io.JsonReader;


public class HttpPostReq {

	
	
	
	HttpPost createConnectivity(String restUrl)
    {
        HttpPost post = new HttpPost(restUrl);
        //String authHeader = "ads_summer2016team4" + "wHud6eRPYhPbheYxEz5CQGF6NDWUqjbOHmPnUJwsnxlF8AefgPx3IrM9EXt0BfGqcEbYy8kl0mmrpvdW7nMzGA=="; // api key
        String authHeader = "Bearer " + "l7NcXyQtRmDaoCxVX0GhIsI5mx0ek0bUKu/Wqs/3D5q+QVOz+txikcGmIb2Fqppq1A/YRo87MGyjlWE2MseglA=="; // api key
        post.setHeader("AUTHORIZATION", authHeader);
        post.setHeader("Content-Type", "application/json");
        post.setHeader("Accept", "application/json");
        post.setHeader("X-Stream" , "true");
        return post;
    }
	
	
	HttpPost createConnectivity2(String restUrl)
    {
        HttpPost post = new HttpPost(restUrl);
        String authHeader = "Bearer " + "e0oswmFDkUElyZ+Tb8UAuzdW2TwPdU/7VWhaHbjgNLe/4Y6GLrwg8YoQCUZ5fUg8jRUJ3ea8tY+E3evq0o+vhQ=="; // api key
        post.setHeader("AUTHORIZATION", authHeader);
        post.setHeader("Content-Type", "application/json");
        post.setHeader("Accept", "application/json");
        post.setHeader("X-Stream" , "true");            
        return post;
    }
	
	
	
	
     
    @SuppressWarnings("finally")
    JSONArray executeReq(String jsonData, HttpPost httpPost)
    {
    	JSONArray predict = null;
        try{
             predict = executeHttpRequest(jsonData, httpPost);
            
        }
        catch (UnsupportedEncodingException e){
            System.out.println("error while encoding api url : "+e);
        }
        catch (IOException e){
            System.out.println("ioException occured while sending http request : "+e);
        }
        catch(Exception e){
            System.out.println("exception occured while sending http request : "+e);
        }
        finally{
            httpPost.releaseConnection();
            return predict;
        }
    }
     
    
    
    
    
    JSONArray executeHttpRequest(String jsonData,  HttpPost httpPost)  throws UnsupportedEncodingException, IOException
    {
        HttpResponse response=null;
        String line = "";
        StringBuffer result = new StringBuffer();
        httpPost.setEntity(new StringEntity(jsonData));
        HttpClient client = HttpClientBuilder.create().build();
        response = client.execute(httpPost);
        System.out.println("Post parameters : " + jsonData );
        System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
        System.out.println("Response  : " + response);
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        while ((line = reader.readLine()) != null){ result.append(line); }
        System.out.println("result:  "+ result.toString());
     
        JSONObject jsonObj = new JSONObject(result.toString());
        JSONObject obj2 = (JSONObject)jsonObj.get("Results");
        System.out.println("obj2 -> "+jsonObj.get("Results"));
        
        JSONObject obj3 = (JSONObject)obj2.get("output1");
        System.out.println("obj3 -> "+obj2.get("output1"));
        
        JSONObject obj4 = (JSONObject)obj3.get("value");
        System.out.println("obj4 -> "+obj3.get("value"));
        
        JSONArray returnscore = (JSONArray)obj4.get("Values");
//        System.out.println("returnscore -> "+returnscore);
//        
//        int totalT = returnscore.length();
//        int[] statuslist = new 
//        for(int i=0;i<totalT;i++){
//        	JSONArray values = (JSONArray)returnscore.get(1);
//            System.out.println(values.getString(0));
//        	
//        }
//        JSONArray values = (JSONArray)returnscore.get(1);
//        System.out.println(values.getString(0));
//        
//        
//        String output = values.getString(0);
//        
//        
//        System.out.println(output);
        return returnscore;
        
    }

}
