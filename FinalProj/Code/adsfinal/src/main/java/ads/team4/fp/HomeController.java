package ads.team4.fp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.microsoft.azure.storage.*;
import com.microsoft.azure.storage.file.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for the application home page.
 */




@Controller
public class HomeController {
	// Configure the connection-string with your values

	
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {

		
		return "home";
	}
	
	@RequestMapping(value = "ViewDetails.htm", method = RequestMethod.POST)
	public ModelAndView View(HttpServletRequest hsr, HttpServletResponse response,Locale locale, Model model) throws Exception {
		ModelAndView mv= new ModelAndView();
		String full = hsr.getParameter("details");
		String zipcode = full.substring(full.length()-6, full.length()-1);
		String name = full.substring(0, full.length()-7);
		mv.addObject("zipcode", zipcode);
		mv.addObject("name", name);
		mv.setViewName("NewFile");
		
		
		return mv;
	}

	@RequestMapping(value = "Estimate.htm", method = RequestMethod.POST)
	public ModelAndView Estimate(HttpServletRequest hsr, HttpServletResponse response,Locale locale, Model model) throws Exception {
		ModelAndView mv= new ModelAndView();
		String full = hsr.getParameter("details2");
		String zipcode = full.substring(full.length()-6, full.length()-1);
		String storageConnectionString =
			    "DefaultEndpointsProtocol=http;" +
			    "AccountName=finalprojbostonrent;" +
			    "AccountKey=jmVu9SMLvCgz82JnmtO9BddKdyGeRAVPsFGzFTgvBQ4NOE/Pd2Rz3mZ+riGOG6eICvH6rtI1YDlNjsVr46zcSQ==";

		// Use the CloudStorageAccount object to connect to your storage account
		try {
		    CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);

		    
		CloudFileClient fileClient = storageAccount.createCloudFileClient();
		
		// Get a reference to the file share
		CloudFileShare share = fileClient.getShareReference("asd");
		//Get a reference to the root directory for the share.
		CloudFileDirectory sampleDir = share.getRootDirectoryReference();

		//Get a reference to the directory that contains the file


		//Get a reference to the file you want to download
		CloudFile file = sampleDir.getFileReference("2015-10-18.geojson");
		
		mv.addObject("file", file.downloadText());
		//Write the contents of the file to the console.

	} catch (Exception invalidKey) {
	    // Handle the exception
		System.out.println("12");
	}// Create the file storage client.
	
		mv.addObject("zipcode", zipcode);
		mv.addObject("bed", hsr.getParameter("bedroom").equals("0") ? 1 : hsr.getParameter("bedroom"));
		mv.setViewName("NewFile");
		
		
		return mv;
	}
	
	
	
	
	@RequestMapping(value = "map.htm", method = RequestMethod.POST)
	public ModelAndView Map(HttpServletRequest hsr, HttpServletResponse response,Locale locale, Model model) throws Exception {
		ModelAndView mv= new ModelAndView();

		/*URL url = null;
		String strUrl = "http://www.jefftk.com/apartment_prices/apts-1363604521.txt";
		try {
		url = new URL(strUrl);
		} catch (MalformedURLException e2) {
		e2.printStackTrace();
		}
		InputStream is = null;
		try {
		is = url.openStream();
		} catch (IOException e1) {
		e1.printStackTrace();
		}
		OutputStream os = null;
		String txt = "\\dsb.txt";
		try {
		os = new FileOutputStream(hsr.getParameter("folder")+txt);
		int bytesRead = 0;
		byte[] buffer = new byte[8192];
		
		while ((bytesRead = is.read(buffer, 0, 8192)) != -1) {
			System.out.println(bytesRead);
		os.write(buffer, 0, bytesRead);
		}
		System.out.println("���سɹ���"+strUrl);
		} catch (FileNotFoundException e) {
		mv.setViewName("buffererror");
		} catch (IOException e) {
		e.printStackTrace();
		}
		String geoJson = "{\"type\": \"FeatureCollection\",\"features\": [";
		try {
            Scanner in = new Scanner(new File(hsr.getParameter("folder")+txt));
            
            while (in.hasNextLine()) {
                String str = in.nextLine();
                String strr = str.trim();
    	        String[] abc = strr.split("[\\p{Space}]+");
    	        geoJson+="{\"type\": \"Feature\",\"geometry\": {\"type\": \"Point\",\"coordinates\":  [ ";
    	        geoJson+=abc[3];
    	        geoJson+=",";
    	        geoJson+=abc[4];
    	        geoJson+="]},\"properties\": {\"Bedroom\":";
    	        geoJson+=abc[1];
    	        geoJson+=",\"Price\":";
    	        geoJson+=abc[0];
    	        geoJson+="}},";
            }
            geoJson = geoJson.substring(0, geoJson.length()-1);
            geoJson+="]}";
            
            File f = new File(hsr.getParameter("folder")+"\\2013-02-18.geojson");
            OutputStream out = null ;
            out = new FileOutputStream(f);
            byte b[] = geoJson.getBytes();
            out.write(b);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
		System.out.print(hsr.getParameter("folder"));
		mv.addObject("url", hsr.getParameter("folder"));*/
		
		//		logger.info("Welcome home! The client locale is {}.", locale);
//		
//		Date date = new Date();
//		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
//		
//		String formattedDate = dateFormat.format(date);
//		
//		model.addAttribute("serverTime", formattedDate );
		
		mv.setViewName("home");
		
		
		return mv;
	}

}
