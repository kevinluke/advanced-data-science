package ads.team4.fp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;





public class HttpPostReqDetail {

	
	
	
	HttpPost createConnectivitydetail(String restUrl)
    {
        HttpPost post = new HttpPost(restUrl);
        String authHeader = "Bearer " + "WGTP/HGYYP5Q607ExF6zC6J4ziJ8MfdAGlLyBqSD0kky1rs7whT3x3tyvY56QnJAkdCcHBq9tUrfrZlxfPWsmg=="; // api key
        post.setHeader("AUTHORIZATION", authHeader);
        post.setHeader("Content-Type", "application/json");
        post.setHeader("Accept", "application/json");
        post.setHeader("X-Stream" , "true");
        return post;
    }

	
	
	
     
    @SuppressWarnings("finally")
    String executeReqdetail(String jsonData, HttpPost httpPost)
    {
    	String predict = null;
        try{
             predict = executeHttpRequestdetail(jsonData, httpPost);
            
        }
        catch (UnsupportedEncodingException e){
            System.out.println("error while encoding api url : "+e);
        }
        catch (IOException e){
            System.out.println("ioException occured while sending http request : "+e);
        }
        catch(Exception e){
            System.out.println("exception occured while sending http request : "+e);
        }
        finally{
            httpPost.releaseConnection();
            return predict;
        }
    }
     
    
    
    
    
    
    String executeHttpRequestdetail(String jsonData,  HttpPost httpPost)  throws UnsupportedEncodingException, IOException
    {
        HttpResponse response=null;
        String line = "";
        StringBuffer result = new StringBuffer();
        httpPost.setEntity(new StringEntity(jsonData));
        HttpClient client = HttpClientBuilder.create().build();
        response = client.execute(httpPost);
        System.out.println("Post parameters : " + jsonData );
        System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
        System.out.println("Response  : " + response);
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        while ((line = reader.readLine()) != null){ result.append(line); }
        System.out.println("result:  "+ result.toString());
     
        JSONObject jsonObj = new JSONObject(result.toString());
        JSONObject obj2 = (JSONObject)jsonObj.get("Results");
        System.out.println("obj2 -> "+jsonObj.get("Results"));
        
        JSONObject obj3 = (JSONObject)obj2.get("output1");
        System.out.println("obj3 -> "+obj2.get("output1"));
        
        JSONObject obj4 = (JSONObject)obj3.get("value");
        System.out.println("obj4 -> "+obj3.get("value"));
        
        JSONArray returnscore = (JSONArray)obj4.get("Values");
        System.out.println("returnscore -> "+returnscore);
        
        String s2 = returnscore.toString();
        s2 = s2.substring(3,10);	
        
        System.out.println("price -> "+s2);
        
        
        return s2;
        
    }
    
    
    
    
    
    

}
